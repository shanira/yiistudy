<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170718_125000_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
			'name' => $this->String(),
			'username' => $this->String(),
			'password' => $this->String(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
