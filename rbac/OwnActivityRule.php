<?php
namespace app\rbac;

use app\models\User;
use app\models\Activity;

use yii\rbac\Rule;
use Yii; 

class OwnActivityRule extends Rule
{

	public $name = 'OwnActivityRule';

	public function execute($user, $item, $params)
	{
		
		if(isset($_GET['id'])){
				$userCategory = User::findOne($user);
				$activityCategory = Activity::findOne($_GET['id']);

				if(isset($userCategory) && isset($activityCategory)){
					if($userCategory->categoryId == $activityCategory->categoryId)
						return true;
				} 
			}
		
			return false;
	}
}
		