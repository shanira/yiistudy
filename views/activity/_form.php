<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\Status;
/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
	<!--?= $form->field($model, 'categoryId')->textInput() ?-->
	<?php if (\Yii::$app->user->can('deleteActivity', ['user' =>$model]) ){ ?>
	<?= $form->field($model, 'categoryId')-> dropDownList(Category::getCategory()) ?> 
	<?php } ?>
   
    <!--?= $form->field($model, 'statusId')->textInput() ?-->
	<!--?= $form->field($model, 'statusId')->
				dropDownList(Status::getStatus()) ?--> 
		
	<!--if it's new record (create)-> it's hide the under form and send '2' to the datebase, else (update) -> its show option of statusId-->
	
	<?php if($model->isNewRecord){ ?>
			<div style="display:none;"> <?= $form->field($model, 'statusId')->textInput(['value'=>2]) ?> </div>
		<?php }else{ ?>
			<?= $form->field($model, 'statusId')-> dropDownList(Status::getStatus()) ?>
	<?php } ?>
	
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
